# Android Development Curriculum - Udacity

The section below includes the majority of Udacity's Android-related courses.

All of the [**courses can be taken for free**](http://imgur.com/a/GoLJF); although the paid Nanodegree programs do offer additional benefits.

&nbsp;

-----

&nbsp;

## Curriculum Outline

| Level        | Group       | Course                                                                                                                           | Duration   |
| :---:        | :---:        | :---                                                                                                                             | :---     |
| Beginner     | Core        | [Android Basics: UI](https://www.udacity.com/course/android-basics-user-interface--ud834) &nbsp; ☆¹                              | 2 Weeks  |
| Beginner     | Core        | [Android Basics: User Input](https://www.udacity.com/course/android-basics-user-input--ud836) &nbsp; ☆¹                          | 4 Weeks  |
| Beginner     | Core        | [Android Basics: Multiscreen Apps](https://www.udacity.com/course/android-basics-multi-screen-apps--ud839) &nbsp; ☆¹             | 8 Weeks  |
| Beginner     | Core        | [Android Basics: Networking](https://www.udacity.com/course/android-basics-networking--ud843) &nbsp; ☆¹                          | 5 Weeks  |
| Beginner     | Core        | [Android Basics: Data Storage](https://www.udacity.com/course/android-basics-data-storage--ud845) &nbsp; ☆¹                      | 8 Weeks  |
| Beginner     | Core        | [How to create \<anything\> in Android](https://www.udacity.com/course/how-to-create-anything-in-android--ud802)                 | 4 Weeks  |
| Beginner     | Core        | [UX Design for Mobile Developers](https://www.udacity.com/course/ux-design-for-mobile-developers--ud849)                         | 6 Weeks  |
| Intermediate | Core        | [Developing Android Apps](https://www.udacity.com/course/developing-android-apps--ud853) &nbsp; ☆²                               | 4 Weeks |
| Advanced     | Core        | [Advanced Android App Development](https://www.udacity.com/course/advanced-android-app-development--ud855) &nbsp; ☆²             | 6 Weeks  |
| Intermediate | Services    | [Google Location Services on Android](https://www.udacity.com/course/google-location-services-on-android--ud876-1)               | 2 Weeks  |
| Intermediate | Services    | [Google Analytics for Android](https://www.udacity.com/course/google-analytics-for-android--ud876-2)                             | 2 Weeks  |
| Intermediate | Services    | [Monetize Your Android App with Ads](https://www.udacity.com/course/monetize-your-android-app-with-ads--ud876-3)                 | 2 Weeks  |
| Intermediate | Services    | [Add Google Maps to your Android App](https://www.udacity.com/course/add-google-maps-to-your-android-app--ud876-4)               | 2 Weeks  |
| Intermediate | Services    | [Add Google Sign-In to your Android Apps](https://www.udacity.com/course/add-google-sign-in-to-your-android-apps--ud876-5)       | 2 Weeks  |
| Advanced     | Core        | [Gradle for Android and Java](https://www.udacity.com/course/gradle-for-android-and-java--ud867) &nbsp; ☆²                       | 6 Weeks  |
| Intermediate | Core        | [Material Design for Android Developers](https://www.udacity.com/course/material-design-for-android-developers--ud862) &nbsp; ☆² | 4 Weeks  |
| Intermediate | Games       | [2D Game Development with libGDX](https://www.udacity.com/course/2d-game-development-with-libgdx--ud405)                         | 8 Weeks  |
| Intermediate | Games       | [How to Make a Platformer Using libGDX](https://www.udacity.com/course/how-to-make-a-platformer-using-libgdx--ud406)             | 8 Weeks  |
| Advanced     | Core        | [Android Performance](https://www.udacity.com/course/android-performance--ud825)                                                 | 4 Weeks  |
| Intermediate | Core        | [Firebase in a Weekend: Android](https://www.udacity.com/course/firebase-in-a-weekend-by-google-android--ud0352)                 | 3 Days  |
| Intermediate | Core        | [Firebase Analytics: Android](https://www.udacity.com/course/firebase-analytics-android--ud354)                                  | 2 Days  |
| Advanced     | Core        | [Developing Scalable Apps in Java](https://www.udacity.com/course/developing-scalable-apps-in-java--ud859)                       | 8 Weeks  |
| Advanced     | Platforms   | [Android TV and Google Cast Development](https://www.udacity.com/course/android-tv-and-google-cast-development--ud875B)          | 1 Week   |
| Advanced     | Platforms   | [Android Wear Development](https://www.udacity.com/course/android-wear-development--ud875A) &nbsp;                               | 2 Weeks  |
| Advanced     | Platforms   | [Android Auto Development](https://www.udacity.com/course/android-auto-development--ud875C)                                      | 1 Week   |

&nbsp;

\* *Courses that are followed by* ☆¹ *are part of the [**Android Basics**](https://www.udacity.com/course/android-basics-nanodegree-by-google--nd803) Nanodegree program.*
\* *Courses that are followed by* ☆² *are part of the [**Android Developer**](https://www.udacity.com/course/android-developer-nanodegree-by-google--nd801) Nanodegree program.*
\* *Course lengths are based on Udacity's presumed allocation of 6 hours per week.*
\* *Originally Forked From [**Enteleform**](https://github.com/Enteleform/-RES-/blob/master/%5BLinks%5D/%5BAndroid%5D%20Udacity%20Curriculum.md)*